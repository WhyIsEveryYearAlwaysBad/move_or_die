-- Initial code for setup.
move_or_die = { -- Namespace for mod.
   world_config = nil -- Mod-specific settings local to the world.
}

dofile(minetest.get_modpath(minetest.get_current_modname()) .. "/src/constants.lua") -- Constant variables that should not be changed.

-- Initialize world-local settings if the config file (<world path>/move_or_die.conf) doesn't exist.
-- If the mod-specific world-local configuration file (<world path>/move_or_die.conf) exists; then load it and replace any missing settings with their default value (from `move_or_die.world_settings_template`).
do
   local settings_file = io.open(move_or_die.world_config_path, "r")
   if not settings_file
   then
      minetest.log("info", "[Move or Die] Could not load world-local configuration file '" .. move_or_die.world_config_path .. "'. Generation of the world-local configuration file will begin.")
      -- Generate move_or_die.conf.
      settings_file = io.open(move_or_die.world_config_path, "a")
      for setting, template in pairs(move_or_die.world_settings_template) do
	 settings_file:write("# " .. template.comment .. "\n" .. setting .. " = " .. tostring(template.default_value) .. "\n\n")
      end
      settings_file:close()
      move_or_die.world_config = Settings(move_or_die.world_config_path)
   else -- Check the options to ensure that they are set.
      move_or_die.world_config = Settings(move_or_die.world_config_path)
      for setting, template in pairs(move_or_die.world_settings_template) do
	 if move_or_die.world_config:get(setting) == nil
	 then
	    minetest.log("warning", "[Move or Die] Setting '" .. setting .. "' could not be loaded from world-local configuration file '" .. move_or_die.world_config_path .. ". Falling back to default value '" .. tostring(template.default_value) .. "'!")
	    move_or_die.world_config:set(setting, tostring(template.default_value))
	 end
      end
      settings_file:close()
   end
end

--- Issue a notice if the damage is disabled.
if not minetest.settings:get("enable_damage") then
   minetest.log("warning", "[Move or Die] Damage is disabled; kill mechanisms will not work!")
end

dofile(minetest.get_modpath("move_or_die").."/src/game.lua") -- Load game code.

-- `move_or_die:ignore` is a privilege that excludes a player from being killed by lack of movement.
minetest.register_privilege("move_or_die:ignore",
			    { description = "Excludes player from participating in this minigame-mod.",
			      give_to_singleplayer = false,
			      on_grant = function(granted_name) move_or_die.remove_participant_player(minetest.get_player_by_name(granted_name)) end,
			      on_revoke = function (revoked_name) move_or_die.add_participant_player(minetest.get_player_by_name(revoked_name)) end
			    }
)

minetest.register_on_joinplayer(move_or_die.add_participant_player)
minetest.register_on_respawnplayer(function (player)
      minetest.after(tonumber(move_or_die.world_config:get("grace_period")), move_or_die.update_participant, player, nil)
end)
