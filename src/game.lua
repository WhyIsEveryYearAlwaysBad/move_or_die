-- game.lua stores the code for the minigame to function.

-- Updates the state of the participant. The function loops itself unless the participant has the `move_or_die:ignore` privilege.
-- @participant - Player that does not have move_or_die:ignore.
-- @kill_job_ref - Reference to the cancel function of the killer job (that kills the player).
function move_or_die.update_participant(participant, kill_job_ref)
   local meta_ref = participant:get_meta()
   if move_or_die.check_movement(participant)
   then -- Player moved
      if kill_job_ref ~= nil then
	 kill_job_ref:cancel()
	 kill_job_ref = nil
      end
   else -- Player didn't move.
      if kill_job_ref == nil then
	 kill_job_ref = minetest.after(move_or_die.world_config:get("idle_death_delay"), participant.set_hp, participant, -1)
      end
   end
   if not minetest.check_player_privs(participant, "move_or_die:ignore") and participant:get_hp() > 0.0 then
      minetest.after(0.1, move_or_die.update_participant, participant, kill_job_ref)
   end
end

function move_or_die.check_movement(player, deltatime) -- Checks if player is moving and returns a boolean.
   local abs_velocity = vector.apply(player:get_velocity(), math.abs)
   local controls = player:get_player_control()
   local t_velocity_threshold = vector.from_string(move_or_die.world_config:get("velocity_movement_threshold"))
   -- If the player is pressing inputs to move and controls
   if move_or_die.world_config:get_bool("player_input_is_movement") and (controls.up or controls.down or controls.left or controls.right) then
      return true
   elseif abs_velocity.x >= t_velocity_threshold.x
      or abs_velocity.y >= t_velocity_threshold.y
      or abs_velocity.z >= t_velocity_threshold.z
   then
      return true
   else -- Player is idle.
      return false
   end
end

function move_or_die.add_participant_player(player) -- Adds a player as a new participant, *unless if they have the `move_or_die:ignored` privilege.*
   if not minetest.check_player_privs(player, "move_or_die:ignore") then
      minetest.after(tonumber(move_or_die.world_config:get("grace_period")), move_or_die.update_participant, player, nil)
   end
end

function move_or_die.remove_participant_player(participant)
end
