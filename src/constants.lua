move_or_die.world_config_path = minetest.get_worldpath() .. "/move_or_die.conf"

-- A table of settings with their default values. **IT SHOULD STAY CONSTANT; DO NOT MODIFY IN RUNTIME!**
-- The keys in `world_settings_template` should be the setting name; the value should be a table that stores the setting's default value (like 42) and the comment.
move_or_die.world_settings_template = {
   player_input_is_movement = {
      default_value = false,
      comment = "Player controls tag a participant as moving."
   },
   grace_period = {
      default_value = 10.0,
      comment = "The grace period (in seconds) for connecting or (re)spawned participants before the kill trigger engages."
   },
   idle_death_delay = {
      default_value = 2.0,
      comment = "The delay for a physically idle participant's death."
   },
   velocity_movement_threshold = {
      default_value = vector.new(0.1, 0.1, 0.1),
      comment = "Velocity threshold that determines a player is moving if their velocity is above this vector."
   }
}
