#+title: Project Objectives

* Past

** [X] Store minigame settings in world-local settings file instead of minetest.conf.

Reasoning behind this change is that setting options that influence the mod behavior on a global scale could cause confusion.

* Current

* Future

** [ ] Add privilege to exclude player from the mod.

If the privilege is granted, then the player with the privilege is removed from the ~participant_list~.

If the privilege is revoked, then the player will be added to the ~participant_list~ and gain a grace period.

