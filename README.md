# Move or Die

Move or Die is a minigame mod that forces players to move above a certain velocity; otherwise they will die!

## Contributing

Move or Die uses [`git`](https://git-scm.com/); so send me patch files. (Pull requests are an abomination created by GitHub.)